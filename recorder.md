# Recorder

Recorders are originally woodwind musical instruments, but nowadays they are widely spread as plastic instruments used to teach children about musical theory. 

## Types of recorder flutes

Most people when they hear recorder they think of the soprano recorder, but other sizes exist as well. When the recorder is longer and wider it's sound is lower and when it is shorter the sound is higher. This is the same way as the playing the air column.

We have 4 usual types of the recorder flutes which are soprano, alto, tenor and bass, but in addition to these we have more types like piccolo or contrabass.

![VariousRecorderFlutes](https://upload.wikimedia.org/wikipedia/commons/1/18/VariousRecorderFlutes.jpg)

Source: https://en.wikipedia.org/wiki/File:VariousRecorderFlutes.jpg
    



 
